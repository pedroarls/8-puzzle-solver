cmake_minimum_required(VERSION 3.7.2)

project(IA_TP1)

#file(GLOB SOURCES "*.cpp")

SET(SOURCES  "Utility/PriorityQueue.cpp" "State/State.cpp" "Node/Node.cpp" "Puzzle/Puzzle.cpp" "Solvers/Solvers.cpp" "main.cpp")
SET(CMAKE_CXX_FLAGS "-std=c++17")

add_executable(IA_TP1 ${SOURCES})

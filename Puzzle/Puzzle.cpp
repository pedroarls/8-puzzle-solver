#include "Puzzle.hpp"
#include <iomanip>

//Funcoes publicas
Puzzle::Puzzle(string puzzle)
{
    this->puzzle = puzzle;
}


bool Puzzle::operator==(Puzzle puzzle){
    if(this->puzzle == puzzle.getPuzzle()){
        return true;
    }
    else{
        return false;
    }
}

Puzzle& Puzzle::operator=(Puzzle& other){
    puzzleSize = other.puzzleSize;
    puzzle = other.puzzle;
    return *this;
}


void Puzzle::showPuzzle(){
    int size = this->puzzleSize * this->puzzleSize;
    for (int i = 0; i < size; i++)
    {
        cout << setw(2) << this->puzzle.at(i);

        if ((i + 1) % this->puzzleSize == 0)
        {
            cout << endl;
        }
    }
    cout<<endl;
}

int Puzzle::missplacedCells(){
    int size = this->puzzleSize * this->puzzleSize;
    int missplacedCount = 0;

    for (int i = 0; i < size - 1; i++)
    {
        //The i-th tile of the puzzle should be i+1
        //Except for the last one that should be 0
        if (this->puzzle.at(i) != to_string(i + 1).at(0))
        {
            // cout << setw(2) << i;
            missplacedCount++;
        }
    }
    if (this->puzzle.at(8) != to_string(0).at(0))
    {
        missplacedCount++;
    }

    return missplacedCount;
}

int Puzzle::manhattanDistance(){
    int size = this->puzzleSize * this->puzzleSize;
    int x0, x1, y0, y1;
    int md = 0;
    for (int i = 0; i < size - 1; i++){
        //The i-th tile of the puzzle should be i+1
        //Except for the last one that should be 0
        if (this->puzzle.at(i) != to_string(i + 1).at(0) && this->puzzle.at(i)!= to_string(0).at(0)){
            int n = (this->puzzle.at(i)) - '0'; //converte char pra inteiro
            int correct_x = (n-1) /3;
            int correct_y = (n-1) %3;
            int actual_x = i/3;this->puzzle.at(i) -1 ;
            int actual_y = i%3;
        
            md = md + (abs(correct_x - actual_x)+ abs(correct_y+actual_y));
        }
    }
    if (this->puzzle.at(size - 1) != to_string(0).at(0)){
        int n = (this->puzzle.at(size-1)) - '0'; //converte char pra inteiro
        int correct_x = (n - 1) /3;
        int correct_y = (n - 1) %3;
        int actual_x = (size-1)/3;
        int actual_y = (size-1)%3;
        // cout <<  (abs(correct_x - actual_x)+ abs(correct_y+actual_y)) << endl;

        md = md + (abs(correct_x - actual_x)+ abs(correct_y+actual_y));    
    }
    return md;
}


int Puzzle::getSize()
{
    return this->puzzleSize;
}

string Puzzle::getPuzzle() const
{
    return this->puzzle;
}

tuple<int, int> Puzzle::getEmpty()
{
    int i = 0, j = 0;

    for (i = 0; i < (this->puzzleSize * this->puzzleSize) && (this->puzzle.at(i) != to_string(EMPTY).at(0)); i++);

    j = i % 3;
    i = i / 3;

    //  cout << i << " " << j << endl;
    tuple<int, int> position(i, j);
    return position;
}

bool Puzzle::moveDown()
{
    auto empty = this->getEmpty();
    int emptyPositionRow = get<0>(empty);
    int emptyPositionCol = get<1>(empty);

    if ((emptyPositionRow - 1) >= 0)
    {
        char temp = this->puzzle.at((emptyPositionRow - 1) * this->getSize() + emptyPositionCol);

        this->puzzle.at((emptyPositionRow - 1) * this->getSize() + emptyPositionCol) = to_string(EMPTY).at(0);
        this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol) = temp;

        // this->showPuzzle();

        return true;
    }
    else
    {
        // this->showPuzzle();
        return false;
    }
}

bool Puzzle::moveUp()
{
    auto empty = this->getEmpty();
    int emptyPositionRow = get<0>(empty);
    int emptyPositionCol = get<1>(empty);

    if ((emptyPositionRow + 1) < 3)
    {
        int temp = this->puzzle.at((emptyPositionRow + 1) * this->getSize() + emptyPositionCol);

        this->puzzle.at((emptyPositionRow + 1) * this->getSize() + emptyPositionCol) = to_string(EMPTY).at(0);
        this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol) = temp;

        // this->showPuzzle();

        return true;
    }
    else
    {
        // this->showPuzzle();
        return false;
    }
}

bool Puzzle::moveRight()
{
    auto empty = this->getEmpty();
    int emptyPositionRow = get<0>(empty);
    int emptyPositionCol = get<1>(empty);

    if ((emptyPositionCol - 1) >= 0)
    {
        int temp = this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol - 1);

        this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol - 1) = to_string(EMPTY).at(0);
        this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol) = temp;

        return true;
    }
    else
    {
        return false;
    }
}

bool Puzzle::moveLeft()
{
    auto empty = this->getEmpty();
    int emptyPositionRow = get<0>(empty);
    int emptyPositionCol = get<1>(empty);

    if ((emptyPositionCol + 1) < 3)
    {
        int temp = this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol + 1);

        this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol + 1) =  to_string(EMPTY).at(0);
        this->puzzle.at((emptyPositionRow) * this->getSize() + emptyPositionCol) = temp;

        // this->showPuzzle();

        return true;
    }
    else
    {
        // this->showPuzzle();
        return false;
    }
}

//Funcoes privadas
void Puzzle::gerarPuzzleAleatorio()
{
    int size = this->puzzleSize * this->puzzleSize;
    // vector<int> orderedBoard = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    vector<int> orderedBoard;

    for(int k=0; k<size; k++){
        orderedBoard.push_back(k);
    }

    srand(time(NULL));
    for (int i = 0; i < size; i++)
    {
        int randomPosition = rand() % orderedBoard.size();
        // this->puzzle[i] = orderedBoard[randomPosition];
        if(i>=size){
            this->puzzle.append(to_string(i));
        }
        else{
            this->puzzle.at(i) = to_string(orderedBoard[randomPosition]).at(0);
        }

        orderedBoard.erase(orderedBoard.begin() + randomPosition);
    }
}

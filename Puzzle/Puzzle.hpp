#ifndef PUZZLE_HPP
#define PUZZLE_HPP

#define EMPTY 0

#include <iostream>
#include <vector>
#include <tuple>

const unsigned int MISSPLACED_CELLS = 1;
const unsigned int MANHATTAN_DISTANCE = 2;
const unsigned int EUCLIDEAN_DISTANCE = 3;

using namespace std;

class Puzzle{
    private:
        int puzzleSize = 3;
        // vector<int> puzzle;
        string puzzle;

        void gerarPuzzleAleatorio();
   
    public:
        int& operator()(const unsigned &, const unsigned &);
        const int& operator()(const unsigned&, const unsigned &) const;
        bool operator==(Puzzle  puzzle);
        Puzzle& operator=(Puzzle& puzzle);

        Puzzle(string puzzle="000000000");
        Puzzle(const Puzzle&) = default;
        void showPuzzle();
        int missplacedCells();
        int manhattanDistance();
        int getSize();
        // vector<int>& getPuzzle();
        string getPuzzle() const;
        tuple<int,int> getEmpty();

        bool moveUp();
        bool moveDown();
        bool moveLeft();
        bool moveRight();

};

#endif


#ifndef MOVE_HPP
#define MOVE_HPP

#include<map>
#include<string>
#include<iostream>

enum  MOVE{
    UP = 1,
    DOWN = 2,
    LEFT = 3,
    RIGHT = 4,
    UNDEFINED = 0 //only valid for the initial state
};
#endif
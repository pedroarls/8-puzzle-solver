#ifndef NODE_HPP
#define NODE_HPP
#include "../State/State.hpp"
#include <vector>
#include <iostream>

using namespace std;

class Node
{
    int cost;
    int depth;
    State state;
    Node* parent;

    static vector<Node*> generated_nodes;

    public:
        Node() = default;
        Node(const Node&) = default;
        Node(Node* parent, int cost, State s);
        void setParent(Node* parent);
        Node* getParent();
        void destroy_tree();
        
        vector<Node> getChildren();
        State getState() const;
        int getCost() const;
        void setCost(int cost);
        int getDepth() const;
        bool operator==(Node node);
        Node& operator=(Node node);

        friend ostream& operator<<(ostream& os,const Node& node){
            os << "=================" << endl;
            node.state.getPuzzle().showPuzzle();
            os << "Cost: "<< node.cost<< endl;
            os << "=================" << endl;
        }
};
#endif
#include "Node.hpp"

vector<Node*> Node::generated_nodes;

Node::Node(Node *parent, int cost, State s) : parent(parent), cost(cost), state(s) {
    if(parent==NULL){
        depth = 0;
    }
    else{
        depth = parent->getDepth() +1;
    }
}

void Node::setParent(Node *parent)
{
    this->parent = parent;
}

Node* Node::getParent(){
    return this->parent;
}


State Node::getState() const
{
    return this->state;
}

int Node::getCost() const{
    return this->cost;
}

void Node::setCost(int cost){
    this->cost = cost;
}


int Node::getDepth() const{
    return this->depth;
}

void Node::destroy_tree(){
    while(!this->generated_nodes.empty()){
        Node * n = this->generated_nodes.back();
        this->generated_nodes.pop_back();
        free(n);
    }
}


vector<Node> Node::getChildren(){
    vector<Node> children;

    Node *node = new Node(this->parent, this->cost, this->state);
    this->generated_nodes.push_back(node);
    Node nodeUp = Node(this->parent, this->cost+1, this->state);
    Node nodeDown = Node(this->parent, this->cost+1, this->state);
    Node nodeRight = Node(this->parent, this->cost+1, this->state);
    Node nodeLeft = Node(this->parent, this->cost+1, this->state);
    bool moved;

    //Se o estado original eh resultante de uma acao down, nao realiza a operaco UP
    //Isso eh feito para evitar alguns estados repetidos
    if(this->getState().getAction()!=DOWN){
        
        Puzzle p = nodeUp.getState().getPuzzle();
        moved = p.moveUp();
        if (moved)
        {   
            State s = State(p, UP);
            nodeUp = Node(node,this->cost+1, s);
            children.push_back(nodeUp);
        }
    }
   
    if(this->getState().getAction()!=UP){
        Puzzle p = nodeDown.getState().getPuzzle();
        moved = p.moveDown();
        if (moved)
        {   
            State s = State(p, DOWN);
            nodeDown = Node(node,this->cost+1, s);
            nodeDown.getState().getPuzzle().moveDown();
           
            children.push_back(nodeDown);
        }
    }

    if(this->getState().getAction()!=LEFT){
        Puzzle p = nodeRight.getState().getPuzzle();
        moved = p.moveRight();
        if (moved)
        {   
            State s = State(p, RIGHT);
            nodeRight = Node(node,this->cost+1, s);
            nodeRight.getState().getPuzzle().moveRight();

            children.push_back(nodeRight);
        }
    }

    if(this->getState().getAction()!=RIGHT){
        Puzzle p = nodeLeft.getState().getPuzzle();
        moved = p.moveLeft();
        if (moved)
        {   
            State s = State(p, LEFT);
            nodeLeft = Node(node,this->cost+1, s);
            nodeLeft.getState().getPuzzle().moveLeft();

            children.push_back(nodeLeft);
        }
    }

    return children;
}

bool Node::operator==(Node node) 
{
    if (node.state == this->state)
    {
        return true;
    }
    else
    {
        return false;
    }
}

Node& Node::operator=(Node node){
    cost = node.cost;
    state = node.state;
    parent = node.parent;
    depth = node.depth;
    return *this;
}


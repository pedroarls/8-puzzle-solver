#ifndef SOLVERS_HPP
#define SOLVERS_HPP

#include<iostream>
#include<vector>
#include <utility>

#include "../Puzzle/Puzzle.hpp"
#include "../Node/Node.hpp"

using namespace std;
class Solvers{
    private:
        // vector<int> goal;
        string goal;
        Puzzle puzzle;
        int steps;
        int expandedNodes;
        int exploredNodes;
        int nodesInFrontier;
        string fileName;
    
    public:
        Solvers(Puzzle puzzle, string fileName);

        void setGoal();
        bool goalTest(Puzzle& p1);
        void printSolution(Node solution);

        void bfs();
        void ids();
        void uniformCostSearch();
        void greedySearch(unsigned int heuristic = MISSPLACED_CELLS);
        void aStarSearch(unsigned int heuristic = MISSPLACED_CELLS);
        void hillClimbing(unsigned int heuristic = MISSPLACED_CELLS);
    
    private:
        pair<bool,Node> dls(Node root, int limit);
        int heuristicCost(Puzzle p,unsigned int heuristic = MISSPLACED_CELLS);

};

#endif
#include "Solvers.hpp"
#include "../Node/Node.hpp"
#include "../Puzzle/Move.hpp"

#include <algorithm>
#include <list>
#include <vector>
#include <chrono>
#include <ctime>
#include <set>
#include <queue> //fila de prioridade
#include <unordered_map>

#include "../Utility/PriorityQueue.hpp"

using namespace std::chrono;

struct cost_comparison
{
    bool operator()(const Node &x, const Node &y) const
    {
        return x.getCost() > y.getCost();
    }
};

struct depth_comparison
{
    bool operator()(const Node &x, const Node &y)
    {
        return x.getDepth() > y.getDepth();
    }
};

Solvers::Solvers(Puzzle puzzle, string fileName) : puzzle(puzzle), fileName(fileName)
{
    this->setGoal();
}

void Solvers::setGoal()
{
    this->goal = "000000000";
    int size = this->puzzle.getSize() * this->puzzle.getSize();

    // this->goal.resize(size);
    // for(int i=0; i<size; i++){
    //     if (i >= goal.size())
    //     {
    //         this->goal.append(to_string(i));
    //     }
    //     else
    //     {
    //         this->goal.at(i) = to_string(i).at(0);
    //     }
    // }

    for (int i = 0; i < size; i++)
    {
        if (i >= goal.size())
        {
            this->goal.append(to_string(i));
        }
        else
        {
            this->goal.at(i) = to_string(i + 1).at(0);
        }
    }
    goal.at(size - 1) = to_string(0).at(0);
}

bool Solvers::goalTest(Puzzle &p1)
{
    if (p1.getPuzzle() == this->goal)
        return true;
    else
        return false;
}

void Solvers::printSolution(Node solution)
{
    vector<string> path;
    vector<Node> pathNodes;
    Node *node = new Node(solution.getParent(), 0, solution.getState());

    while (node != NULL)
    {
        if (node->getState().getAction() == UP)
        {
            path.push_back("UP");
        }
        if (node->getState().getAction() == DOWN)
        {
            path.push_back("DOWN");
        }
        if (node->getState().getAction() == RIGHT)
        {
            path.push_back("RIGHT");
        }
        if (node->getState().getAction() == LEFT)
        {
            path.push_back("LEFT");
        }
        pathNodes.push_back(*node);
        node = (node->getParent());
    }

    std::cout << std::endl
              << "SOLUTION" << std::endl;
    for (int i = path.size() - 1; i >= 0; i--)
    {
        std::cout << path[i] << std::endl;
        pathNodes[i].getState().getPuzzle().showPuzzle();
    }

    solution.destroy_tree();

    return;
}

void Solvers::bfs(){
    Node solution;
    std::cout << "BREADTH FIRST SEARCH" << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    std::cout << "Initial state" << std::endl;
    this->puzzle.showPuzzle();

    auto start = clock();
    this->steps = 0;
    this->expandedNodes = 0;

    //cria um estado inicial com o puzzle a ser resolvido, e acao indefinida
    //ate o momento nenhuma das acoes UP, DOWN, LEFT ou RIGHT foram executdas
    State initialState = State(this->puzzle, UNDEFINED);
    Node raiz = Node(NULL, 0, initialState);
    bool foundSolution = false;

    Puzzle initialPuzzle = initialState.getPuzzle();
    if (this->goalTest(initialPuzzle))
    {
        std::cout << "Solucao encontrada" << std::endl;
        // initialPuzzle.showPuzzle();
        solution = raiz;
        std::cout << std::endl;
    }
    else
    {
        list<Node> frontier;
        std::unordered_map<std::string, bool> explored;
        // set<string> explored;

        frontier.push_back(raiz);
        std::string puzzle = raiz.getState().getPuzzle().getPuzzle();
        explored[puzzle] = false;

        do
        {
            if (frontier.empty())
            {
                std::cout << "Fronteira vazia. Nenhuma solucao encontrada" << std::endl;
                return;
            }
            else
            {
                //retira o no da fronteira e o expande
                Node node = frontier.front();
                frontier.erase(frontier.begin());

                vector<Node> children = node.getChildren();
                expandedNodes = expandedNodes + 1;
                explored[node.getState().getPuzzle().getPuzzle()] = true;

                for (auto child : children)
                {
                    State s = child.getState();
                    bool state_is_in_frontier_or_explored = explored.find(s.getPuzzle().getPuzzle()) == explored.end() ? false : true;

                    if (!state_is_in_frontier_or_explored)
                    {
                        Puzzle nodePuzzle = child.getState().getPuzzle();
                        if (this->goalTest(nodePuzzle))
                        {
                            std::cout << "Uma solucao foi encontrada!" << std::endl;
                            // this->printSolution(child);
                            solution = child;
                            foundSolution = true;
                            steps = child.getCost();
                            break;
                        }
                        frontier.push_back(child);
                        explored[child.getState().getPuzzle().getPuzzle()] = false;
                    }
                }
            }

        } while (!frontier.empty() && !foundSolution);
    }

    auto stop = clock();
    auto duration = float(stop - start) / CLOCKS_PER_SEC;

    this->printSolution(solution);
    //precisao de 15 casas decimais
    std::cout.precision(15);
    std::cout << std::endl;
    std::cout << "Tempo total de execucao: " << duration << " segundos" << std::endl;
    std::cout << "Profundidade da busca: " << solution.getDepth() << std::endl;
    std::cout << "Quantidade de nos expandidos:" << expandedNodes << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;
    return;
}

void Solvers::ids()
{
    Node solution;
    int depth = 0;
    std::pair<bool, Node> result;
    this->expandedNodes = 0;
    this->exploredNodes = 0;
    this->steps = 0;

    auto start = clock();

    State initialState = State(this->puzzle, UNDEFINED);
    Node raiz = Node(NULL, 0, initialState);

    while (result.first == false)
    {
        auto r = dls(raiz, depth);
        result.first = r.first;
        result.second = r.second;
        solution = r.second;
        depth++;
    }

    auto stop = clock();
    auto duration = float(stop - start) / CLOCKS_PER_SEC;
    this->steps = result.second.getCost();

    std::cout << "ITERATIVE DEEPENING SEARCH" << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    std::cout << "Initial state" << std::endl;
    this->puzzle.showPuzzle();

    this->printSolution(solution);

    //precisao de 15 casas decimais
    std::cout.precision(15);
    std::cout << std::endl;
    std::cout << "Tempo total de execucao: " << duration << " segundos" << std::endl;
    std::cout << "Profundidade da busca " << solution.getDepth() << std::endl;
    std::cout << "Quantidade de nos expandidos:" << this->expandedNodes << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;
    return;
}

pair<bool, Node> Solvers::dls(Node root, int limit){
    std::pair result = make_pair(false, root);
    vector<Node> nodes;
    std::unordered_map<std::string, bool> explored;

    nodes.push_back(root);
    explored[root.getState().getPuzzle().getPuzzle()] = false;

    while (true)
    {
        if (nodes.empty())
        {
            return result;
        }

        //Pilha, sempre retira o ultimo no inserido
        Node node = nodes.back();
        nodes.pop_back();
        explored[node.getState().getPuzzle().getPuzzle()] = true;

        Puzzle p = node.getState().getPuzzle();
        if (this->goalTest(p))
        {
            result.first = true;
            result.second = node;
            return result;
        }
        else if (node.getDepth() <= limit)
        {
            this->expandedNodes++;
            vector<Node> children = node.getChildren();
             
            for (auto child : children)
            {
                Puzzle nodePuzzle = child.getState().getPuzzle();
                if (this->goalTest(nodePuzzle))
                {
                    std::cout << "Uma solucao foi encontrada!" << std::endl;
                    result.first = true;
                    result.second = child;
                    return result;
                }
                State s = child.getState();
                bool is_in_explored_or_frontier = explored.find(s.getPuzzle().getPuzzle()) == explored.end() ? false : true;

                // if(is_in_explored_or_frontier){
                //     if(!explored[s.getPuzzle().getPuzzle()]){
                nodes.push_back(child);
                explored[s.getPuzzle().getPuzzle()] = false;
                //     }   
                // }
            }
        }
    }

    return result;
}

void Solvers::uniformCostSearch(){
    Node solution;
    std::cout << "UNIFORM COST SEARCH" << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    std::cout << "Initial state" << std::endl;
    this->puzzle.showPuzzle();

    auto start = clock();
    this->steps = 0;
    this->expandedNodes = 0;

    //cria um estado inicial com o puzzle a ser resolvido, e acao indefinida
    //ate o momento nenhuma das acoes UP, DOWN, LEFT ou RIGHT foram executdas
    State initialState = State(this->puzzle, UNDEFINED);
    Node raiz = Node(NULL, 0, initialState);
    bool foundSolution = false;

    Puzzle initialPuzzle = initialState.getPuzzle();
    if (this->goalTest(initialPuzzle))
    {
        solution = raiz;
    }
    else
    {
        //monta a fila de prioridade
        vector<Node> frontier;
        std::make_heap(frontier.begin(), frontier.end(), depth_comparison());
        // set<string> explored;

        //insere no heap
        frontier.push_back(raiz);
        // std::push_heap(frontier.begin(), frontier.end(),depth_comparison());

        std::unordered_map<std::string, bool> explored;
        std::string puzzle = raiz.getState().getPuzzle().getPuzzle();
        explored[puzzle] = false;

        while (!frontier.empty() && !foundSolution)
        {
            Node node = frontier.front();
            std::pop_heap(frontier.begin(), frontier.end(), depth_comparison());
            frontier.pop_back();

            Puzzle p = node.getState().getPuzzle();
            if (this->goalTest(p))
            {
                solution = node;
            }
            else
            {
                //Adiciona no a lista de estados explorados
                explored[node.getState().getPuzzle().getPuzzle()] = true;

                //Expande no
                vector<Node> children = node.getChildren();
                this->expandedNodes = this->expandedNodes + 1;
                for (auto child : children)
                {
                    State s = child.getState();
                    bool state_is_in_frontier_or_explored = explored.find(s.getPuzzle().getPuzzle()) == explored.end() ? false : true;

                    if (!state_is_in_frontier_or_explored)
                    {
                        Puzzle nodePuzzle = child.getState().getPuzzle();
                        if (this->goalTest(nodePuzzle))
                        {
                            std::cout << "Uma solucao foi encontrada!" << std::endl;
                            solution = child;
                            foundSolution = true;
                            steps = child.getCost();
                            break;
                        }
                        frontier.push_back(child);
                        std::push_heap(frontier.begin(), frontier.end(), depth_comparison());
                        explored[child.getState().getPuzzle().getPuzzle()] = false;
                        // update_heap = true;
                    }
                    else
                    {
                        bool state_is_in_frontier_with_higher_cost = false;
                        if (state_is_in_frontier_or_explored)
                        {
                            state_is_in_frontier_with_higher_cost = !explored[s.getPuzzle().getPuzzle()] && (node.getCost() > child.getCost());
                        }

                        if (state_is_in_frontier_with_higher_cost)
                        {
                            auto searchInFrontier = find(frontier.begin(), frontier.end(), child);
                            Node nodeInFrontier = *(searchInFrontier);
                            frontier.insert(searchInFrontier, child);
                            explored[child.getState().getPuzzle().getPuzzle()] = false;
                            std::make_heap(frontier.begin(), frontier.end(), depth_comparison());
                        }
                    }
                }
            }
        }
    }
    auto stop = clock();
    auto duration = float(stop - start) / CLOCKS_PER_SEC;
    this->printSolution(solution);
    //precisao de 15 casas decimais
    std::cout.precision(15);
    std::cout << std::endl;
    std::cout << "Tempo total de execucao: " << duration << " segundos" << std::endl;
    std::cout << "Profundidade da busca: " << solution.getDepth() << std::endl;
    std::cout << "Quantidade de nos expandidos:" << this->expandedNodes << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;
    return;
}

void Solvers::greedySearch(unsigned int heuristic){
    Node solution;
    std::cout << "GREDDY BEST FIRST SEARCH" << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    std::cout << "Initial state" << std::endl;
    this->puzzle.showPuzzle();

    auto start = clock();
    this->steps = 0;
    this->expandedNodes = 0;

    //cria um estado inicial com o puzzle a ser resolvido, e acao indefinida
    //ate o momento nenhuma das acoes UP, DOWN, LEFT ou RIGHT foram executdas
    State initialState = State(this->puzzle, UNDEFINED);
    Node raiz = Node(NULL, 0, initialState);

    raiz.setCost(raiz.getDepth() + this->heuristicCost(raiz.getState().getPuzzle(), heuristic));
    bool foundSolution = false;

    Puzzle initialPuzzle = initialState.getPuzzle();
    if (this->goalTest(initialPuzzle))
    {
        solution = raiz;
    }
    else
    {
        //monta a fila de prioridade
        vector<Node> frontier;
        std::make_heap(frontier.begin(), frontier.end(), cost_comparison());

        //lista de estados explorados
        std::unordered_map<std::string, bool> explored;

        //coloca a raiz na lista aberta
        frontier.push_back(raiz);

        //no esta na fronteira e nao foi explorado
        std::string puzzle = raiz.getState().getPuzzle().getPuzzle();
        explored[puzzle] = false;

        //enquanto houverem nos na fronteira e a solucao nao houver sido encontrada
        while (!frontier.empty() && !foundSolution){
            //retira da lista aberta o no com o menor custo
            Node node = frontier.front();
            std::pop_heap(frontier.begin(), frontier.end(), cost_comparison());
            frontier.pop_back();

            //se encontrou a solucao
            Puzzle p = node.getState().getPuzzle();
            if (this->goalTest(p))
            {
                solution = node;
                foundSolution=true;
            }
            else
            {
                //Gera os sucessores
                vector<Node> children = node.getChildren();
                this->expandedNodes = this->expandedNodes + 1;

                for (auto child : children)
                {
                    State s = child.getState();
                    child.setCost(this->heuristicCost(child.getState().getPuzzle(), heuristic));

                    bool state_is_in_frontier_or_explored = explored.find(s.getPuzzle().getPuzzle()) == explored.end() ? false : true;

                    if (!state_is_in_frontier_or_explored)
                    {

                        Puzzle nodePuzzle = child.getState().getPuzzle();
                        if (this->goalTest(nodePuzzle))
                        {
                            std::cout << "Uma solucao foi encontrada!" << std::endl;
                            solution = child;
                            foundSolution = true;
                            steps = child.getCost();
                            break;
                        }
                        frontier.push_back(child);
                        std::push_heap(frontier.begin(), frontier.end(), cost_comparison());
                        explored[child.getState().getPuzzle().getPuzzle()] = false;
                    }
                    else
                    {
                        bool state_is_in_frontier_with_higher_cost = false;
                        if (state_is_in_frontier_or_explored)
                        {
                            state_is_in_frontier_with_higher_cost = !explored[s.getPuzzle().getPuzzle()] && (node.getDepth() > child.getDepth());
                        }

                        if (state_is_in_frontier_with_higher_cost)
                        {
                            auto searchInFrontier = find(frontier.begin(), frontier.end(), child);
                            Node nodeInFrontier = *(searchInFrontier);
                            frontier.insert(searchInFrontier, child);
                            explored[child.getState().getPuzzle().getPuzzle()] = false;
                            std::make_heap(frontier.begin(), frontier.end(), cost_comparison());
                        }
                    }

                    //Adiciona no a lista de estados explorados
                    explored[node.getState().getPuzzle().getPuzzle()] = true;
                }
            }
        }
    }
    auto stop = clock();
    auto duration = float(stop - start) / CLOCKS_PER_SEC;
    this->printSolution(solution);
    //precisao de 15 casas decimais
    std::cout.precision(15);
    std::cout << std::endl;
    std::cout << "Tempo total de execucao: " << duration << " segundos" << std::endl;
    std::cout << "Profundidade da busca: " << solution.getDepth() << std::endl;
    std::cout << "Quantidade de nos expandidos:" << this->expandedNodes << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;
    return;
}

void Solvers::aStarSearch(unsigned int heuristic)
{
    Node solution;
    std::cout << "A STAR SEARCH" << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    std::cout << "Initial state" << std::endl;
    this->puzzle.showPuzzle();

    auto start = clock();
    this->steps = 0;
    this->expandedNodes = 0;

    //cria um estado inicial com o puzzle a ser resolvido, e acao indefinida
    //ate o momento nenhuma das acoes UP, DOWN, LEFT ou RIGHT foram executdas
    State initialState = State(this->puzzle, UNDEFINED);
    Node raiz = Node(NULL, 0, initialState);

    raiz.setCost(raiz.getDepth() + this->heuristicCost(raiz.getState().getPuzzle(), heuristic));
    bool foundSolution = false;

    Puzzle initialPuzzle = initialState.getPuzzle();
    if (this->goalTest(initialPuzzle))
    {
        solution = raiz;
    }
    else
    {
        //monta a fila de prioridade
        vector<Node> frontier;
        std::make_heap(frontier.begin(), frontier.end(), cost_comparison());

        //lista de estados explorados
        std::unordered_map<std::string, bool> explored;

        //coloca a raiz na lista aberta
        frontier.push_back(raiz);

        //no esta na fronteira e nao foi explorado
        std::string puzzle = raiz.getState().getPuzzle().getPuzzle();
        explored[puzzle] = false;

        //enquanto houverem nos na fronteira e a solucao nao houver sido encontrada
        while (!frontier.empty() && !foundSolution)
        {
            //retira da lista aberta o no com o menor custo
            Node node = frontier.front();
           

            //se encontrou a solucao
            Puzzle p = node.getState().getPuzzle();
            if (this->goalTest(p))
            {
                solution = node;
                foundSolution = true;
                break;
            }
            else
            {
                //Retira da fronteira
                std::pop_heap(frontier.begin(), frontier.end(), cost_comparison());
                frontier.pop_back();

                //Adiciona no a lista de estados explorados
                explored[node.getState().getPuzzle().getPuzzle()] = true;

                //Gera os sucessores
                vector<Node> children = node.getChildren();
                this->expandedNodes = this->expandedNodes + 1;

                for (auto child : children)
                {
                    State s = child.getState();
                    child.setCost(child.getDepth() + this->heuristicCost(child.getState().getPuzzle(), heuristic));

                    bool state_is_in_frontier_or_explored = 
                        explored.find(s.getPuzzle().getPuzzle()) == explored.end() ? false : true;

                    if (!state_is_in_frontier_or_explored)
                    {

                        Puzzle nodePuzzle = child.getState().getPuzzle();
                        if (this->goalTest(nodePuzzle))
                        {
                            std::cout << "Uma solucao foi encontrada!" << std::endl;
                            solution = child;
                            foundSolution = true;
                            steps = child.getCost();
                            break;
                        }
                        frontier.push_back(child);
                        std::push_heap(frontier.begin(), frontier.end(), cost_comparison());
                        explored[child.getState().getPuzzle().getPuzzle()] = false;
                    }
                    else
                    {
                        bool state_is_in_frontier_with_higher_cost = false;
                        if (state_is_in_frontier_or_explored)
                        {
                            state_is_in_frontier_with_higher_cost = 
                                !explored[s.getPuzzle().getPuzzle()] && (node.getDepth()+1 < child.getDepth());
                        }

                        if (state_is_in_frontier_with_higher_cost)
                        {
                            auto searchInFrontier = find(frontier.begin(), frontier.end(), child);
                            Node nodeInFrontier = *(searchInFrontier);
                            frontier.insert(searchInFrontier, child);
                            explored[child.getState().getPuzzle().getPuzzle()] = false;
                            std::make_heap(frontier.begin(), frontier.end(), cost_comparison());
                        }
                    }

                  
                }
            }
        }
    }
    auto stop = clock();
    auto duration = float(stop - start) / CLOCKS_PER_SEC;
    this->printSolution(solution);
    //precisao de 15 casas decimais
    std::cout.precision(15);
    std::cout << std::endl;
    std::cout << "Tempo total de execucao: " << duration << " segundos" << std::endl;
    std::cout << "Profundidade da busca: " << solution.getDepth() << std::endl;
    std::cout << "Quantidade de nos expandidos:" << this->expandedNodes << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;
    return;
}

void Solvers::hillClimbing(unsigned int heuristic){

    Node solution;
    std::cout << "HILL CLIMBING SEARCH" << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    std::cout << "Initial state" << std::endl;
    this->puzzle.showPuzzle();

    auto start = clock();
    this->steps = 0;
    this->expandedNodes = 0;

    //cria um estado inicial com o puzzle a ser resolvido, e acao indefinida
    //ate o momento nenhuma das acoes UP, DOWN, LEFT ou RIGHT foram executdas

    State initialState = State(this->puzzle, UNDEFINED);
    Node raiz = Node(NULL, 0, initialState);

    raiz.setCost(raiz.getDepth() + this->heuristicCost(raiz.getState().getPuzzle(), heuristic));
    bool foundSolution = false;
    solution = raiz;

    Puzzle initialPuzzle = initialState.getPuzzle();
    if (this->goalTest(initialPuzzle))
    {
        solution = raiz;
    }
    else
    {
        vector<Node> frontier;
        vector<Node> stack;
        std::unordered_map<std::string, bool> explored;

        frontier.push_back(raiz);
        int count = 0;
        Node currentNode = frontier.front();

        do{

            Puzzle p = currentNode.getState().getPuzzle();
            if (this->goalTest(p)){
                solution = currentNode;
                foundSolution = true;
            }
            else{
                bool state_is_explored = !(explored.find(p.getPuzzle()) == explored.end());

                if (!state_is_explored){
                    vector<Node> children = currentNode.getChildren();
                    this->expandedNodes = this->expandedNodes + 1;
                    explored[p.getPuzzle()] = true;
                    // cout << node.getCost() << endl;
                    int plateau = 0;
                    int localMaxima = 0;
                    for (auto child : children){
                        int cost = this->heuristicCost(child.getState().getPuzzle(), heuristic);
                        child.setCost(cost);
                        frontier.push_back(child);
                        std::push_heap(frontier.begin(), frontier.end(), cost_comparison());
                        Puzzle p = child.getState().getPuzzle();
                        if (this->goalTest(p)){
                            solution = child;
                            foundSolution = true;
                        }

                        if (child.getCost() == currentNode.getCost()){
                            stack.push_back(child);
                            plateau++;
                        }
                        else if (child.getCost() > currentNode.getCost()){
                            stack.push_back(child);
                            localMaxima++;
                        }
                    }
                  
                    if (plateau == children.size()){
                         srand(time(NULL));
                        int randomPosition = rand() % stack.size();
                        currentNode = stack.at(randomPosition);
                        stack.erase(stack.begin() + randomPosition);
                        // cout << "plateau  atingido" << endl;
                    }
                    else if (localMaxima == children.size()){   
                        srand(time(NULL));
                        int randomPosition = rand() % stack.size();
                        currentNode = stack.at(randomPosition);
                        stack.erase(stack.begin() + randomPosition);
                    }
                    else if (children.front().getCost() <= currentNode.getCost()){
                        currentNode = children.front();
                        std::pop_heap(frontier.begin(), frontier.end(), cost_comparison());
                        frontier.pop_back();
                    }
                    else{
                        srand(time(NULL));
                        int randomPosition = rand() % frontier.size();
                        currentNode = frontier.at(randomPosition);
                        frontier.erase(frontier.begin() + randomPosition);
                        std::make_heap(frontier.begin(), frontier.end(), cost_comparison());
                    }
                }
                else{
                    currentNode = frontier.front();
                    std::pop_heap(frontier.begin(), frontier.end(), cost_comparison());
                    frontier.pop_back();
                }
            }
        } while (!frontier.empty() && !foundSolution);
    }
    auto stop = clock();
    auto duration = float(stop - start) / CLOCKS_PER_SEC;
    this->printSolution(solution);
    //precisao de 15 casas decimais
    std::cout.precision(15);
    std::cout << std::endl;
    std::cout << "Tempo total de execucao: " << duration << " segundos" << std::endl;
    std::cout << "Profundidade da busca: " << solution.getDepth() << std::endl;
    std::cout << "Quantidade de nos expandidos:" << this->expandedNodes << std::endl;
    std::cout << "====================================" << std::endl;
    std::cout << std::endl;

    return;
}

int Solvers::heuristicCost(Puzzle p, unsigned int heuristic){
    if (heuristic <= MISSPLACED_CELLS){
        return p.missplacedCells();
    }
    else{
        return p.manhattanDistance();
    }
}

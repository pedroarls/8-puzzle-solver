#ifndef PRIORITY_QUEUE
#define PRIORITY_QUEUE

#include <queue>
#include <set>


template<
    class T, 
    class Compare
> class PriorityQueue{
    typedef std::priority_queue<int, std::vector<T>,Compare> queue;

    private:
        std::set<T, Compare> elements;

    public:
        PriorityQueue() = default;

        bool empty(){
            return this->elements.empty();
        }

        void push(const T& val){
            this->elements.insert(val);
        }
        
        const T& top() const{
            auto top = this->elements.begin();
            // auto found = this->elements.find(top);
            return *top;
        }

        void pop(){
            if(!this->elements.empty()) this->elements.erase(this->elements.begin());
        }

        bool contains(const T& val) const{
            bool result = this->elements.find(val) == this->elements.end() ? false : true;
            return result;
        }

    
};

#endif
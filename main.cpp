#ifdef __cplusplus__
#include <cstdlib>
#else
#include <stdlib.h>
#endif

#include <iostream>
#include "Puzzle/Puzzle.hpp"
#include "Solvers/Solvers.hpp"
#include "State/State.hpp"
#include "Node/Node.hpp"

#include <algorithm>
#include <set>
#include <queue>
#include <vector>
#include <iomanip>

#include "Utility/PriorityQueue.hpp"

using namespace std;
const int BREADTH_FIRST_SEARCH = 1;
const int ITERATIVE_DEEPENING_SEARCH = 2;
const int UNIFORM_COST_SEARCH = 3;
const int GREEDY_BEST_FIRST_SEARCH = 4;
const int A_STAR_SEARCH = 5;
const int HILL_CLIMBING = 6;

void initial_screen(int *size, string *puzzle, string* fileName){

  cout << "BEM VINDO AO 8-PUZZLE SOLVER" << endl;
  cout << "======================================" << endl
       << endl;
  // cout << "Primeiro, digite o tamanho do tabuleiro(NXN): ";
  // cin >> *size;
  // if (*size < 3)
  //   *size = 3;
  // cout << endl
  //      << endl;
  cout << "Preciso que voce informe a configuracao do puzzle que deseja resolver.(3x3)" << endl
       << endl;
  cout << "Por exemplo, observe o seguinte tabuleiro:" << endl;
  cout << endl;
  cout << " " << setw(3) << "5 " << setw(2) << "2 " << setw(2) << setw(2) << "0 " << setw(2) << endl;
  cout << " " << setw(2) << "3 " << setw(2) << "6 " << setw(2) << setw(2) << "4 " << setw(2) << endl;
  cout << " " << setw(2) << "1 " << setw(2) << "7 " << setw(2) << setw(2) << "8 " << setw(2) << endl;
  cout << endl;
  cout << "A entrada correspondente seria: 520364178." << endl;
  cout << endl
       << "Vamos la! Digite a configuracao do puzzle a ser resolvido: ";

  cin >> *puzzle;

  // cout << endl
  //      << "Legal!Agora, informe um nome de arquivo para salvar as solucoes encontradas: " << endl;
  // cin >> *fileName;

  cout << endl
       << "Tecle ENTER para continuar ou CTRL+D para encerrar." << endl;

  getchar();
  getchar();

  if (system("CLS"))
    system("clear");
}

bool keep_going(){
  bool keep_going = true;
  string answer;
  cout << "Deseja continuar utilizando o programa (S para sim, N para Nao)? " << endl;
  cin>>answer;

  char c = std::tolower(answer.at(0));
  keep_going = (c == 's') ? true : false;

  if(keep_going)
    if (system("CLS")) system("clear");
  else
    exit(0);
  
  return keep_going;

}

int solve_puzzle_options(int size, string puzzle){
  Puzzle p = Puzzle(puzzle);

  cout << "OK! Agora vamos resolver o problema!" << endl
       << endl;
  cout << "Abaixo tem algumas opcoes de algoritmo para voce. Escolha uma delas e espere o resultado." << endl;

  cout << "****** Algoritmos de busca disponiveis ******" << endl
       << endl;
  cout << "Algoritmos de busca sem informacao" << endl
       << endl;
  cout << " " << setw(5) << BREADTH_FIRST_SEARCH << " - BREADTH FIRST SEARCH" << endl;
  cout << " " << setw(5) << ITERATIVE_DEEPENING_SEARCH << " - ITERATIVE DEEPENING SEARCH " << endl;
  cout << " " << setw(5) << UNIFORM_COST_SEARCH << " - UNIFORM COST SEARCH" << endl;
  cout << endl;

  cout << "Algoritmos de busca com informacao" << endl
       << endl;
  cout << " " << setw(5) << GREEDY_BEST_FIRST_SEARCH << " - GREDDY BEST FIRST SEARCH" << endl;
  cout << " " << setw(5) << A_STAR_SEARCH << " - A* SEARCH" << endl;
  cout << endl;

  cout << "Algoritmos de busca local" << endl
       << endl;
  cout << " " << setw(5) << HILL_CLIMBING << " - HILL CLIMBING" << endl;

  int option;
  cout << endl;
  cout << "Digite o numero do algoritmo desejado: ";
  cin >> option;

  cout << endl;

  if (option <= 0 || option > 6)
  {
    option = 0;
    cout << "Ufa! Essa foi por pouco! Voce digitou um numero fora da faixa disponivel." << endl;
    cout << "Escolhemos a opcao 1 - BREADTH FIRST SEARCH para voce ;)" << endl;
    cout << endl;
  }
  else
  {
    cout << "Voce escolheu a opcao " << option << endl;
  }

  cout << endl << "Legal!Agora vamos resolver o problema." << endl;
  // cout << endl
  //      << "Digite qualquer tecla para continuar ou CTRL+D para encerrar." << endl;

  // getchar();
  // getchar();

  // if (system("CLS"))
  //   system("clear");

  return option;
}

int choose_heuristic(){
  cout<< endl << endl;
  cout << "Esse algoritmo requer um heuristica para construir a solucao." <<endl;
  cout << "Escolha uma dentre as opcoes disponiveis abaixo" << endl;
  cout << " " << setw(5) << "1 - Pecas fora do lugar" << endl;
  cout << " " << setw(5) << "2 - Distancia de Manhattan" << endl;
  cout<< endl;

  int option = 0;
  cout << "Digite a opcao escolhida: ";
  cin >> option;

  if(option <=0 || option >2)
    option = 1;

  return option;
}

int main(){

  int size = 0;
  string puzzle;
  string fileName;
  initial_screen(&size, &puzzle, &fileName);
  do{
    int heuristic = MISSPLACED_CELLS;
    int option = solve_puzzle_options(size, puzzle);

    Puzzle p = Puzzle(puzzle);
    Solvers sol = Solvers(p, fileName);
    cout<<endl<<"Aguarde alguns instantes enquanto o problema é resolvido." <<endl;
    cout<<"Isso pode demorar um pouco, va tomar um cafezinho!" << endl;
    switch (option){
    case BREADTH_FIRST_SEARCH:
      sol.bfs();
      break;

    case ITERATIVE_DEEPENING_SEARCH:
      sol.ids();
      break;

    case UNIFORM_COST_SEARCH:
      sol.uniformCostSearch();
      break;

    case GREEDY_BEST_FIRST_SEARCH:
      heuristic = choose_heuristic();
      sol.greedySearch(heuristic);
      break;

    case A_STAR_SEARCH:
      heuristic = choose_heuristic();
      sol.aStarSearch(heuristic);
      break;

    case HILL_CLIMBING:
      heuristic = choose_heuristic();
      sol.hillClimbing(heuristic);
      break;

    default:
      cout << endl;
      cout << "Algo de errado nao esta certo! Uma opcao desconhecida de algoritmo foi selecionada!" << endl;
      cout << "I give up!" << endl;
      exit(0);
      break;
    }

 
  }while(keep_going());

  // sol.bfs();
  // sol.ids();
  //  sol.uniformCostSearch();
  // sol.greedySearch(MANHATTAN_DISTANCE);
  // sol.aStarSearch(MANHATTAN_DISTANCE);
  // sol.hillClimbing(MANHATTAN_DISTANCE);

  return 0;
}
#include "State.hpp"

State::State(Puzzle puzzle, MOVE action) : puzzle(puzzle), action(action){};

Puzzle State::getPuzzle() const{
    return this->puzzle;
}

MOVE State::getAction(){
    return this->action;
}

void State::show()
{
    cout << this->action << endl;
    this->puzzle.showPuzzle();
}

bool State::operator==(const State& state){
    if(this->puzzle == state.puzzle){
        return true;
    }
    else{
        return false;
    }
}

State& State::operator=(State& state){
    action = state.action;
    puzzle = state.puzzle;
    
    return *this;
}
#ifndef STATE_HPP
#define STATE_HPP

#include "../Puzzle/Move.hpp"
#include "../Puzzle/Puzzle.hpp"

using namespace std;

class State{
    private:
        MOVE action;
        Puzzle puzzle;

    public:


        State() = default;
        State(const State&) = default;
        State(Puzzle puzzle, MOVE action);
        Puzzle getPuzzle() const;
        MOVE getAction();
        void show();
        bool operator==(const State& state);
        State& operator=(State&);

};

#endif